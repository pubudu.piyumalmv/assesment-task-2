

	import javax.swing.*;
	import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
	import java.awt.Color;
	import java.awt.*;

	public class View  extends JFrame {
	    private JTextField nameField;
	    private JButton submitButton;
	    private JLabel label, inputLabel;
	    private JLabel label_version;
	    private ActionListener submitListener;
	    
	    Model modelInterface = new Model();
	    Controller controllerInterface = new Controller(modelInterface, this); 
	    
	    public void View() {
	    	
	    	JFrame frame = new JFrame("JOptionPane showMessageDialog example");

			JOptionPane.showMessageDialog(frame, "Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe. \n" 
					+ "Version 1.0 (c), Kevan Buckley, 2010");
			
			String name = "";
			String name2 = "";
			
			name2 = JOptionPane.showInputDialog("Enter Your Name");
			if (name2 == null) {
				name = "!!Pressed cancel!!";
			}else {
				name = name2;
			}
			
			if(name == "!!Pressed cancel!!") {
				System.exit(0);
			}else {
				
				controllerInterface.sendNameToBackEnd(name);
				
//				data.setPlayerName(name);
//				System.out.printf("%s %s. %s", MultiLinugualStringTable.getMessage(1), data.getPlayerName(),
//				MultiLinugualStringTable.getMessage(2));
				
				JOptionPane.showMessageDialog(frame, MultiLinugualStringTable.getMessage(1) + " " + name + "!! \n " +
						MultiLinugualStringTable.getMessage(2));
				
			}
	        
	    }

	    public String getName() {
	        return nameField.getText();
	    }
	}

