

import java.awt.event.*;

public class Controller {
    private  Model model;
    private  View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

//        view.getSubmitButton().addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                String name = view.getName();
//                model.setName(name);
//            }
//        });
    }
    
    public void showView() {
    	view.View();
    }

	public String getName() {
		return model.getName();
	}  
	
	public void sendNameToBackEnd(String name) {
		model.sendNameToBackEnd(name);
	}
}
