

public class Model {
    private String name;

    public void model() {
    	
    }
    
    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }
    
    public void sendNameToBackEnd(String name) {
    	System.out.printf("%s %s. %s", MultiLinugualStringTable.getMessage(1),
    			name, MultiLinugualStringTable.getMessage(2));
    }
    
  }
