import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AardvarkTest {

	@Test
	public void testGecko() {
		//Test input where x == 32 & 16
		assertEquals(-7, Aardvark.gecko(32 & 16));
		
		//Test input where x < 0
		assertEquals(-7, Aardvark.gecko(-1));
		
		//Test input where x > 0
		assertEquals(-7, Aardvark.gecko(1));
		
	}

}
