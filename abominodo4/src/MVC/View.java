package MVC;

	import javax.swing.*;
	import java.awt.Dimension;
	import java.awt.event.ActionListener;
	import java.awt.Color;
	import java.awt.*;

	public class View  extends JFrame {
	    private JTextField nameField;
	    private JButton submitButton;
	    private JLabel label, inputLabel;
	    private JLabel label_version;
	    private ActionListener submitListener;

	    public View() {
	        this.setTitle("Enter Your Name: ");
	        this.setSize(400, 250);
	        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	        label = new JLabel("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe");
	        label.setForeground(Color.RED);
	        label.setFont(new Font("default", Font.PLAIN, 10));

	        label_version = new JLabel(" Version 1.0 (c), Kevan Buckley, 2010");
	        label_version.setForeground(Color.BLUE);
	        label_version.setFont(new Font("default", Font.PLAIN, 8));

	        nameField = new JTextField();
	        nameField.setPreferredSize(new Dimension(250, 25));

	        submitButton = new JButton("Submit");

	        JPanel content = new JPanel();
	        content.add(label);
	        content.add(label_version);
	        content.add(Box.createRigidArea(new Dimension(0, 50))); 
	        content.add(new JSeparator(JSeparator.HORIZONTAL));
	        content.add(nameField);
	        content.add(submitButton);
	        this.setContentPane(content);
	    }

	    public JButton getSubmitButton() {
	        return submitButton;
	    }

	    public void addSubmitListener(ActionListener listener) {
	        this.submitListener = listener;
	        submitButton.addActionListener(submitListener);
	    }

	    public String getName() {
	        return nameField.getText();
	    }
	}

