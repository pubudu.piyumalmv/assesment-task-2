package MVC;

import java.awt.event.*;

public class Controller {
    private  Model model;
    private  View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

        view.getSubmitButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = view.getName();
                model.setName(name);
            }
        });
    }

	public String getName() {
		return model.getName();
	}  
}
